<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the installation.
 * You don't have to use the web site, you can copy this file to "wp-config.php"
 * and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * Database settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/documentation/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** Database settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', '' );

/** Database username */
define( 'DB_USER', '' );

/** Database password */
define( 'DB_PASSWORD', '' );

/** Database hostname */
define( 'DB_HOST', '' );

/** Database charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The database collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication unique keys and salts.
 *
 * Change these to different unique phrases! You can generate these using
 * the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}.
 *
 * You can change these at any point in time to invalidate all existing cookies.
 * This will force all users to have to log in again.
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '?VS8;}9g8EL:BFHiT_qQ-pdqZsd2O3FLo0VGSAeSzDJ2+JfW{+ryw<Z7Lubg?ufV' );
define( 'SECURE_AUTH_KEY',  '5.7_Lm7w&.J>o6iGs.};>T`W*HQM|uq!oNTf09M{[dOvB331=>.6a9^1R7`MA@8P' );
define( 'LOGGED_IN_KEY',    'j+<8Y}d{uewN%OB8UMQnT `htbv6|Yu -3s./98]bO`F/hD}Uki#Bye6$.yL}xuH' );
define( 'NONCE_KEY',        'V#)wEuGyVN>q#~|aWw0dvHQ6qa 2/@)g`w#=.AmLjVM4D4WLnl==mWlj4MC,.=g)' );
define( 'AUTH_SALT',        '{oit)c[M^PK<*N;zMm*zX|^TZPs4I-Jh!e~({.AZv+yE>{Vt[-#v7Ewu9Er.cPg|' );
define( 'SECURE_AUTH_SALT', 'i`adO,#0s-gPR|*JM=dAtOOe+JuEAyu)j{r#0Ap?%[6XL8oT^Ya&Ool[3$2dKY<~' );
define( 'LOGGED_IN_SALT',   '!Uj@3J/9.vl4ea+1av^SsDk~DEWQS`@I#I/r0$$mMWnr<VuT]f5j0|Rfi e/0)lv' );
define( 'NONCE_SALT',       'voX`y!+e3gr7@z.@&4P&KFWp~~`${V5#Vwd5*wG45YDcAiDAEiXvH|bG&%<?>rn]' );

/**#@-*/

/**
 * WordPress database table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/documentation/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* Add any custom values between this line and the "stop editing" line. */



/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
