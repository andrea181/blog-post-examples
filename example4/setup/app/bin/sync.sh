#!/bin/bash
rsync -a --delete --exclude "wp-content/uploads" /tmp/codebase/ /home/appuser/public_html/
chown appuser:appgroup /home/appuser/public_html/wp-content/uploads/
sudo -u appuser -i php /home/appuser/public_html/wp --path=/home/appuser/public_html config set DB_HOST $DB_HOST
sudo -u appuser -i  php /home/appuser/public_html/wp --path=/home/appuser/public_html config set DB_USER $DB_USER
sudo -u appuser -i php /home/appuser/public_html/wp --path=/home/appuser/public_html config set DB_PASSWORD $DB_PASSWORD
sudo -u appuser -i php /home/appuser/public_html/wp --path=/home/appuser/public_html config set DB_NAME $DB_NAME
sudo -u appuser -i php /home/appuser/public_html/wp --path=/home/appuser/public_html core update-db 
