# example 4: running Wordpress

## Warning
If you have built the images from example3 you can re-use the apache and PHP image.  

## Building the images
The images we're using are all (almost all) local images.
You will find the Dockerfiles in the setup/ directory.  

### Building the apache image
Note, the vhost is setup to answer to example.com domain. 
You can setup any other domain by modifying `apache/root/var/www/conf.d/vhost-user.conf` file.  
```
cd setup/apache
docker build . -t apache:local
```

### Building the php image
```
cd setup/php-fpm
docker build . -t php:redis-storage

```

### Building the wordpress image
```
cd setup/app
docker build . -t wordpress:5.9

```

### Running stuff
You will need to setup the env files for this to work. You can use the one present in this repository.  
One env file is used by the Wordpress container to setup the DB connections variables.  
The .mariadb.env is used by the MariaDB container to setup the DB.  
Note that `MARIADB_USER` and `DB_USER`, `MARIDB_DATABASE` and `DB_NAME`, `MARIADB_PASSWORD` and `DB_PASSWORD` variables need to match. 
Once the images are built you can run `docker compose up`

## Note
In this example we expose two apache conatiners (one on port 81 and one on port 82).  
You will need an LB to balance requests in front of them.  
The easiest thing would be to setup an NGINX container and using [http://nginx.org/en/docs/http/ngx_http_upstream_module.html](http://nginx.org/en/docs/http/ngx_http_upstream_module.html).

## Upgrade
If you want to test the upgrade procedure as described in the post you will need to replace the codebase in `example4/setup/app/codebase` with the codebase of the latest wordpress verson.  
Note that inside the codebase we ship `codebase/wp` file, which is the wp-cli command you will need for the sync.sh script to work.  
Make sure to include this one as well.  
