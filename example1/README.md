# example 1: single application instance with local session storage

## Building the images
The images we're using are all (almost all) local images.
You will find the Dockerfiles in the setup/ directory.  

### Building the apache image
Note, the vhost is setup to answer to example.com domain. 
You can setup any other domain by modifying `apache/root/var/www/conf.d/vhost-user.conf` file.  
```
cd setup/apache
docker build . -t apache:local
```

### Building the php image
```
cd setup/php-fpm
docker build . -t php:localsession

```

### Building the  ppimage
```
cd setup/app
docker build . -t app:1.0

```

### Running stuff
Once the images are built you can run `docker compose up`
