# example3: multiple application instances, redis as session storage

## Warning
If you have built the images from example1 or 2, you can re-use the apache and app image.  
The PHP image will need to be rebuilt as there are some changes.

## Building the images
The images we're using are all (almost all) local images.
You will find the Dockerfiles in the setup/ directory.  

### Building the apache image
Note, the vhost is setup to answer to example.com domain. 
You can setup any other domain by modifying `apache/root/var/www/conf.d/vhost-user.conf` file.  
```
cd setup/apache
docker build . -t apache:local
```

### Building the php image
```
cd setup/php-fpm
docker build . -t php:redis-storage

```

### Building the app image
```
cd setup/app
docker build . -t app:1.0

```

### Running stuff
Once the images are built you can run `docker compose up`

## Note
In this example we expose two apache conatiners (one on port 81 and one on port 82).  
You will need an LB to balance requests in front of them.  
The easiest thing would be to setup an NGINX container and using [http://nginx.org/en/docs/http/ngx_http_upstream_module.html](http://nginx.org/en/docs/http/ngx_http_upstream_module.html).

