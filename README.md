# blog post examples

In this repository, you will find the examples seen in [https://www.sysbee.net/blog/going-up-in-smoke-i-mean-moving-to-the-cloud](https://www.sysbee.net/blog/going-up-in-smoke-i-mean-moving-to-the-cloud).  
Check the README in each example directory  

## Future improvments to this repo
Some examples require that you setup a reverse proxy to be able to try out everthing proprely.  
An NGINX service will be added to the docker-compose files so you don't need to setup this part manually.

